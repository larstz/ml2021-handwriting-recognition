from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import sys

class Window(QMainWindow):

    def __init__(self):
        super().__init__()
        self.title = "Handwriting Recognition"
        self.top = 150
        self.left = 150
        self.width = 500
        self.height = 500
        self.image = QImage(384,96,QImage.Format_Grayscale8)
        self.image.fill(Qt.white)
        pybutton = QPushButton('Click me', self)
        pybutton.resize(100, 32)
        pybutton.move(50, 50)
        pybutton.clicked.connect(self.clickMethod)
        self.InitWindow()

    def InitWindow(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.top, self.left, self.width, self.height)
        self.show()

    def mouseMoveEvent(self, e):
        if self.last_x is None:  # First event.
            self.last_x = e.x()
            self.last_y = e.y()
            return  # Ignore the first time.

        painter = QtGui.QPainter(self.label.pixmap())
        painter.drawLine(self.last_x, self.last_y, e.x(), e.y())
        painter.end()
        self.update()

        # Update the origin for next time.
        self.last_x = e.x()
        self.last_y = e.y()

    def mouseReleaseEvent(self, e):
        self.last_x = None
        self.last_y = None

    def clickMethod(self):
        print('Clicked Pyqt button.')
if __name__ == '__main__':
    App = QApplication(sys.argv)
    window = Window()
    sys.exit(App.exec_())