#!/usr/bin/env python
# coding: utf-8

# In[1]:


import cv2
import numpy as np
import random
import matplotlib.pyplot as plt
import tensorflow as tf
from collections import namedtuple
from typing import Tuple
from multiprocessing.pool import ThreadPool as Pool
from os import getpid
import time
import pandas as pd
from keras.models import Sequential
from keras.layers import Dense, Flatten, Conv2D, MaxPool2D, Dropout, Reshape, LSTM, Bidirectional, Input, BatchNormalization,Lambda
from keras.optimizers import SGD, Adam
from keras.callbacks import ReduceLROnPlateau, EarlyStopping
from keras.utils import to_categorical
from keras_ctcmodel.CTCModel import CTCModel as CTCModel
from keras.preprocessing import sequence
from tensorflow.keras import layers
from tensorflow import keras
import keras.backend as K


# # Getting the paths to lead the dataset

# In[2]:


"""This funtion will probably change in order to make it simpler,
   because there is several structures we will not use"""

Sample = namedtuple('Sample', 'gt_text, file_path')

def load_dataset(data_dir, data_split: float = 0.95) -> None:
    samples = []
    f = open(data_dir + 'gt\\words.txt')
    chars = set()
    bad_samples_reference = ['a01-117-05-02', 'r06-022-03-05', 'p02-109-01-00']  # known broken images in IAM dataset
    for line in f:
    # ignore comment line
        if not line or line[0] == '#':
            continue

        line_split = line.strip().split(' ')
        assert len(line_split) >= 9

        # filename: part1-part2-part3 --> part1/part1-part2/part1-part2-part3.png
        file_name_split = line_split[0].split('-')
        file_name_subdir1 = file_name_split[0]
        file_name_subdir2 = f'{file_name_split[0]}-{file_name_split[1]}'
        file_base_name = line_split[0] + '.png'
        file_name = data_dir + 'img\\' + file_name_subdir1 +'\\'+ file_name_subdir2 +'\\'+ file_base_name

        if line_split[0] in bad_samples_reference:
            print('Ignoring known broken image:', file_name)
            continue

        # GT text are columns starting at 9
        gt_text = ' '.join(line_split[8:])
        chars = chars.union(set(list(gt_text)))

        # put sample into list
        samples.append(Sample(gt_text, file_name))

    # split into training and validation set: 95% - 5%
    split_idx = int(data_split * len(samples))
    train_samples = samples[:split_idx]
    validation_samples = samples[split_idx:]

    # put words into lists
    train_paths = [x.file_path for x in train_samples]
    validation_paths = [x.file_path for x in validation_samples]
    
    # put words into lists
    train_labels = [x.gt_text for x in train_samples]
    test_labels = [x.gt_text for x in validation_samples]

    # list of all chars in dataset
    char_list = sorted(list(chars))
    return train_labels, test_labels, char_list, validation_paths, train_paths


# In[3]:


def getting_img(path, data_augmentation : bool = True, padding : int = 0)-> np.ndarray:
    img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    processed_img = process_img(img, data_augmentation = data_augmentation, padding = padding)
    return processed_img.reshape((32,128,1))
def getting_img_test(path, data_augmentation : bool = False, padding : int = 0)-> np.ndarray:
    img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    processed_img = process_img(img, data_augmentation = data_augmentation, padding = padding)
    return processed_img.reshape((32,128,1))


# # Preprocessing Functions

# In[4]:


"""What it is new about this funtion, is the threshold when converting it into a binary image"""


def process_img(img: np.ndarray, data_augmentation: bool = True, padding: int = 0) -> np.ndarray:
    """Resize to target size, apply data augmentation."""
    img_size = (128,32)
    # there are damaged files in IAM dataset - just use black image instead
    if img is None:
        img = np.zeros(img_size[::-1])

    # data augmentation
    img = img.astype(np.float)
    if data_augmentation:
        # photometric data augmentation
        if random.random() < 0.25:
            def rand_odd():
                return random.randint(1, 3) * 2 + 1
            img = cv2.GaussianBlur(img, (rand_odd(), rand_odd()), 0)
        if random.random() < 0.25:
            img = cv2.dilate(img, np.ones((3, 3)))
        if random.random() < 0.25:
            img = cv2.erode(img, np.ones((3, 3)))

        # geometric data augmentation
        wt, ht = img_size
        h, w = img.shape
        f = min(wt / w, ht / h)
        fx = f * np.random.uniform(0.75, 1.05)
        fy = f * np.random.uniform(0.75, 1.05)

        # random position around center
        txc = (wt - w * fx) / 2
        tyc = (ht - h * fy) / 2
        freedom_x = max((wt - fx * w) / 2, 0)
        freedom_y = max((ht - fy * h) / 2, 0)
        tx = txc + np.random.uniform(-freedom_x, freedom_x)
        ty = tyc + np.random.uniform(-freedom_y, freedom_y)

        # map image into target image
        M = np.float32([[fx, 0, tx], [0, fy, ty]])
        target = np.ones(img_size[::-1]) * 255
        img = cv2.warpAffine(img, M, dsize=img_size, dst=target, borderMode=cv2.BORDER_TRANSPARENT)
        

        # photometric data augmentation
        if random.random() < 0.5:
            img = img * (0.25 + random.random() * 0.75)
        if random.random() < 0.25:
            img = np.clip(img + (np.random.random(img.shape) - 0.5) * random.randint(1, 25), 0, 255)
        if random.random() < 0.1:
            img = 255 - img

    # no data augmentation
    else:
        #if dynamic_width:
            #ht = img_size[1]
            #h, w = img.shape
            #f = ht / h
            #wt = int(f * w + padding)
            #wt = wt + (4 - wt) % 4
            #tx = (wt - w * f) / 2
            #ty = 0
        #else:
        wt, ht = img_size
        h, w = img.shape
        f = min(wt / w, ht / h)
        tx = (wt - w * f) / 2
        ty = (ht - h * f) / 2

        # map image into target image
        M = np.float32([[f, 0, tx], [0, f, ty]])
        target = np.ones([ht, wt]) * 255
        img = cv2.warpAffine(img, M, dsize=(wt, ht), dst=target, borderMode=cv2.BORDER_TRANSPARENT)
        #img = cv2.resize(img,(wt, ht),target,f,f, interpolation=cv2.INTER_AREA)
        # transform image into a binary image
        # global thresholding
        #(_, img) = cv2.threshold(img, 127, 255, cv.THRESH_BINARY + cv.THRESH_OTSU)
        # adaptive local thresholding
        #img = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,16,2)
    # transpose for TF
    #img = cv2.transpose(img)

    # convert to range [0, 1]
    img = img / 255
    return img


# # Model

# initializer = tf.keras.initializers.TruncatedNormal(mean=0.5,stddev=0.1)
# 
# input_0=Input(shape = (128,32))
# 
# input_1 = Reshape((128,32,1))(input_0)
# 
# conv = Conv2D(filters=32, kernel_size=(5, 5), activation='relu', padding = 'same'\
#               ,kernel_initializer=initializer)(input_1)
# max_pool = MaxPool2D(pool_size=(2, 2), strides=2, padding = 'valid')(conv)
# 
# conv2 = Conv2D(filters=64, kernel_size=(5, 5), activation='relu', padding = 'same',kernel_initializer=initializer)(max_pool)
# max_pool2 = MaxPool2D(pool_size=(2, 2), strides=2, padding = 'valid')(conv2)
# 
# conv3 = Conv2D(filters=128, kernel_size=(3, 3), activation='relu', padding = 'same',kernel_initializer=initializer)(max_pool2)
# max_pool3 = MaxPool2D(pool_size=(1, 2), strides=(1,2), padding = 'valid')(conv3)
# 
# conv4 = Conv2D(filters=128, kernel_size=(3, 3), activation='relu', padding = 'same',kernel_initializer=initializer)(max_pool3)
# max_pool4 = MaxPool2D(pool_size=(1, 2), strides=(1,2), padding = 'valid')(conv4)
# 
# conv5 = Conv2D(filters=256, kernel_size=(3, 3), activation='relu', padding = 'same',kernel_initializer=initializer)(max_pool4)
# output_cnn = MaxPool2D(pool_size=(1, 2), strides=(1,2), padding = 'valid')(conv5)
# 
# input_rnn = Reshape((32,256))(output_cnn)
# 
# forward_layer = LSTM(256, return_sequences=True)
# 
# backward_layer = LSTM(256, return_sequences=True,activation='relu',  go_backwards=True)
# 
# output_rnn = Bidirectional(forward_layer, backward_layer = backward_layer, input_shape=(32, 256))(input_rnn)
# 
# transition_layer = Reshape((32,1,512))(output_rnn)
# 
# input_ctc = Conv2D(filters=80, kernel_size=(1, 1), activation='softmax',padding= 'same',\
#                    kernel_initializer = initializer)(transition_layer)
# 
# input_ctc_reshaped = Reshape((32,80))(input_ctc)
# 
# crnn_model = CTCModel([input_0], [input_ctc_reshaped])
# 
# crnn_model.compile(Adam(lr=0.0001))
# 

# In[6]:


inputs = Input(shape=(32,128,1))
 
# convolution layer with kernel size (3,3)
conv_1 = Conv2D(64, (3,3), activation = 'relu', padding='same')(inputs)
# poolig layer with kernel size (2,2)
pool_1 = MaxPool2D(pool_size=(2, 2), strides=2)(conv_1)
 
conv_2 = Conv2D(128, (3,3), activation = 'relu', padding='same')(pool_1)
pool_2 = MaxPool2D(pool_size=(2, 2), strides=2)(conv_2)
 
conv_3 = Conv2D(256, (3,3), activation = 'relu', padding='same')(pool_2)
 
conv_4 = Conv2D(256, (3,3), activation = 'relu', padding='same')(conv_3)
# poolig layer with kernel size (2,1)
pool_4 = MaxPool2D(pool_size=(2, 1))(conv_4)
 
conv_5 = Conv2D(512, (3,3), activation = 'relu', padding='same')(pool_4)
# Batch normalization layer
batch_norm_5 = BatchNormalization()(conv_5)
 
conv_6 = Conv2D(512, (3,3), activation = 'relu', padding='same')(batch_norm_5)
batch_norm_6 = BatchNormalization()(conv_6)
pool_6 = MaxPool2D(pool_size=(2, 1))(batch_norm_6)
 
conv_7 = Conv2D(512, (2,2), activation = 'relu')(pool_6)
 
squeezed = Lambda(lambda x: K.squeeze(x, 1))(conv_7)
 
# bidirectional LSTM layers with units=128
blstm_1 = Bidirectional(LSTM(128, return_sequences=True))(squeezed)
blstm_2 = Bidirectional(LSTM(128, return_sequences=True))(blstm_1)
 
outputs = Dense(80, activation = 'softmax')(blstm_2)

crnn_model = CTCModel([inputs], [outputs])

crnn_model.compile(Adam(lr=0.0001))


# In[7]:


def get_batch(paths, characters, label_data, train : bool, batch_size: int = 100, num_batch: int = 0,):
    # Loadint train data
    if train:
        start_batch = batch_size * num_batch 
        end_batch = batch_size * (num_batch + 1)
        if __name__ == '__main__':
            with Pool(4) as pool:
                result = pool.map(getting_img, paths[start_batch:end_batch])
                pool.terminate()
                pool.join()

        train_data = np.asarray(result)
        #the label entries
        dict_char_list = {}
        for i in range(len(characters)):
            dict_char_list[characters[i]] = i
    
        words = label_data[start_batch: end_batch]
        labels = list()
        for word in words:
            letters = list(word)
            label = list()
            for letter in letters:
                label.append(dict_char_list[letter]) 
            labels.append(label)

        label_train = sequence.pad_sequences(labels, value=float(79), dtype='float32',                                         padding="post", maxlen=21)

        len_train_labels = np.asarray([len(labels[i]) for i in range(len(labels))])
        len_train_data = np.zeros(len(result))+31.
    else:
        if __name__ == '__main__':
            with Pool(4) as pool:
                result = pool.map(getting_img_test, paths)
                pool.terminate()
                pool.join()

        train_data = np.asarray(result)
        #the label entries
        dict_char_list = {}
        for i in range(len(characters)):
            dict_char_list[characters[i]] = i
    
        words = label_data
        labels = list()
        for word in words:
            letters = list(word)
            label = list()
            for letter in letters:
                label.append(dict_char_list[letter]) 
            labels.append(label)

        label_train = sequence.pad_sequences(labels, value=float(79), dtype='float32',                                         padding="post", maxlen=21)

        len_train_labels = np.asarray([len(labels[i]) for i in range(len(labels))])
        #len_train_data = np.asarray([len(result[i]) for i in range(len(result))])
        len_train_data = np.zeros(len(result))+31.
        
    return train_data, label_train, len_train_data, len_train_labels


# In[ ]:


"""i = 0
for label in labels:
    if len(label) == 21:
        print(label)
        img = cv2.imread(train_path[i], cv2.IMREAD_GRAYSCALE)
        print(img.shape)
        plt.imshow(img, cmap='gray', vmin=0, vmax=255)
        plt.show()
    i += 1 
dict_char_list = {}
for i in range(len(char_list)):
    dict_char_list[char_list[i]] = i""" 


# In[8]:


get_ipython().run_cell_magic('time', '', '"""From here, we\'ll use test_path and train_path, they are lists with the paths to the data"""\n\npath = "C:\\\\Users\\\\stive\\\\Desktop\\\\Machine LEarning\\\\IAM Dataset\\\\" # Change it depending on where the data is on your drive\ntrain_labels, test_labels, char_list, test_path, train_path = load_dataset(path)')


# In[9]:


crnn_model.summary()


# In[ ]:


test_data, label_test, len_test_data, len_test_labels = get_batch(paths = test_path,                         characters = char_list, label_data = test_labels, train = False)


# In[ ]:


get_ipython().run_cell_magic('time', '', 'num_batches = 100\nlen_dataset = len(train_path)\nepochs = 1\n#batches = 500\nbatches = int(len_dataset // num_batches)\n\nfor epoch in range(epochs):\n    print("epoch #: ",epoch+1,"/",epochs)\n    for batch in range(batches):\n        print("batch #: ",batch,"/",batches, end = \'\\r\')\n        train_data, label_train, len_train_data, len_train_labels = get_batch(num_batch=batch , paths = train_path, \\\n                                characters = char_list, label_data = train_labels, train = True)\n            \n        crnn_model.train_on_batch(x=[train_data, label_train, len_train_data, len_train_labels],\\\n                                  y=np.zeros(len(len_train_labels)))')


# In[ ]:



#crnn_model.fit(x=[train_data, label_train, len_train_data, len_train_labels], y=np.zeros(len(train_labels)), epochs = 2)
              

eval = crnn_model.evaluate(x=[test_data, label_test, len_test_data, len_test_labels],                            batch_size = 50, metrics=['loss', 'ler', 'ser'])


# In[ ]:


pred = crnn_model.predict([train_data[:100],len_train_data[:100]], max_value=1)


# In[ ]:


print("train data shape: ",test_data.shape)
print("label train data shape: ", label_test.shape)
print("train length shape: ", len_test_data.shape)
print("label length shape: ", len_test_labels.shape)


# In[ ]:


print("trand data shape: ",train_data.shape)
print("label train data shape: ", label_train.shape)
print("train length shape: ", len_train_data.shape)
print("label length shape: ", len_train_labels.shape)


# In[ ]:




