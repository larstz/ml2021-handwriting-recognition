#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import cv2
import numpy as np
import random
import matplotlib.pyplot as plt
import tensorflow as tf
from collections import namedtuple
from typing import Tuple
from multiprocessing.pool import ThreadPool as Pool
import time
import pandas as pd
from keras.layers import Dense, Flatten, Conv2D, MaxPool2D, Dropout, Reshape, LSTM, Bidirectional, Input, BatchNormalization,Lambda
from keras.optimizers import Adam
from keras_ctcmodel.CTCModel import CTCModel as CTCModel
from keras.preprocessing import sequence
from tensorflow.keras import layers
from tensorflow import keras
import keras.backend as K
import os
import edit_distance
import enchant
import glob

def load_dataset(data_dir, data_split: float = 0.95):
    Sample = namedtuple('Sample', 'gt_text, file_path')
    samples = []
    f = open(data_dir + 'gt/words.txt')
    chars = set()
    bad_samples_reference = ['a01-117-05-02', 'r06-022-03-05', 'p02-109-01-00']  # known broken images in IAM dataset
    for line in f:
    # ignore comment line
        if not line or line[0] == '#':
            continue

        line_split = line.strip().split(' ')
        assert len(line_split) >= 9

        # filename: part1-part2-part3 --> part1/part1-part2/part1-part2-part3.png
        file_name_split = line_split[0].split('-')
        file_name_subdir1 = file_name_split[0]
        file_name_subdir2 = f'{file_name_split[0]}-{file_name_split[1]}'
        file_base_name = line_split[0] + '.png'
        file_name = data_dir + 'img/' + file_name_subdir1 +'/'+ file_name_subdir2 +'/'+ file_base_name

        if line_split[0] in bad_samples_reference:
            print('Ignoring known broken image:', file_name)
            continue

        # GT text are columns starting at 9
        gt_text = ' '.join(line_split[8:])
        chars = chars.union(set(list(gt_text)))

        # put sample into list
        samples.append(Sample(gt_text, file_name))    

    # split into training and validation set: 95% - 5%
    split_idx = int(data_split * len(samples))
    
    train_samples = samples[:split_idx]
    validation_samples = samples[split_idx:]

    # list of all chars in dataset
    char_list = sorted(list(chars))
    return train_samples, validation_samples, char_list

def getting_img(path, data_augmentation : bool = True)-> np.ndarray:
    img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    processed_img = process_img(img, data_augmentation = data_augmentation)
    return processed_img.reshape((32, 128, 1))
def getting_img_test(path, data_augmentation : bool = False)-> np.ndarray:
    img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    processed_img = process_img(img, data_augmentation = data_augmentation)
    return processed_img.reshape((32, 128, 1))
def get_prediction(pred : np.ndarray, char_list : list):
    dict_num_char = dict()
    i = 0
    for char in char_list:
        dict_num_char[i] = char
        i += 1
    predictions = list()   
    for row in range(len(pred)): 
        guess = ""
        for j in pred[row]:
            if (j != -1 and j != 79):
                guess += dict_num_char[j]
        predictions.append(guess)
    return np.asarray(predictions)

def process_img(img: np.ndarray, data_augmentation: bool = True) -> np.ndarray:
    """Resize to target size, apply data augmentation."""
    img_size = (128,32)
    # there are damaged files in IAM dataset - just use black image instead
    if img is None:
        img = np.zeros(img_size[::-1])

    # data augmentation
    
    if data_augmentation:
        img = img.astype(np.float)
        # photometric data augmentation
        if random.random() < 0.25:
            def rand_odd():
                return random.randint(1, 3) * 2 + 1
            img = cv2.GaussianBlur(img, (rand_odd(), rand_odd()), 0)
        if random.random() < 0.25:
            img = cv2.dilate(img, np.ones((3, 3)))
        if random.random() < 0.25:
            img = cv2.erode(img, np.ones((3, 3)))

        # geometric data augmentation
        wt, ht = img_size
        h, w = img.shape
        f = min(wt / w, ht / h)
        fx = f * np.random.uniform(0.75, 1.05)
        fy = f * np.random.uniform(0.75, 1.05)

        # random position around center
        txc = (wt - w * fx) / 2
        tyc = (ht - h * fy) / 2
        freedom_x = max((wt - fx * w) / 2, 0)
        freedom_y = max((ht - fy * h) / 2, 0)
        tx = txc + np.random.uniform(-freedom_x, freedom_x)
        ty = tyc + np.random.uniform(-freedom_y, freedom_y)

        # map image into target image
        M = np.float32([[fx, 0, tx], [0, fy, ty]])
        target = np.ones(img_size[::-1]) * 255
        img = cv2.warpAffine(img, M, dsize=img_size, dst=target, borderMode=cv2.BORDER_TRANSPARENT)
        

        # photometric data augmentation
        if random.random() < 0.5:
            img = img * (0.25 + random.random() * 0.75)
        if random.random() < 0.25:
            img = np.clip(img + (np.random.random(img.shape) - 0.5) * random.randint(1, 25), 0, 255)
        if random.random() < 0.1:
            img = 255 - img

    # no data augmentation
    else:
        # transform image into a binary image
        # global thresholding
        #_, img = cv2.threshold(img, 127, 255, cv.THRESH_BINARY + cv.THRESH_OTSU)
        # adaptive local thresholding
        #img = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,15,2)
        img = img.astype(np.float)
        wt, ht = img_size
        h, w = img.shape
        f = min(wt / w, ht / h)
        tx = (wt - w * f) / 2
        ty = (ht - h * f) / 2

        # map image into target image
        M = np.float32([[f, 0, tx], [0, f, ty]])
        target = np.ones([ht, wt]) * 255
        img = cv2.warpAffine(img, M, dsize=(wt, ht), dst=target, borderMode=cv2.BORDER_TRANSPARENT)
        

    # convert to range [0, 1]
    img = img / 255
    return img

def create_model():
    inputs = Input(shape=(32,128,1))
 
    # convolution layer with kernel size (3,3)
    conv_1 = Conv2D(64, (3,3), activation = 'relu', padding='same')(inputs)
    # poolig layer with kernel size (2,2)
    pool_1 = MaxPool2D(pool_size=(2, 2), strides=2)(conv_1)
 
    conv_2 = Conv2D(128, (3,3), activation = 'relu', padding='same')(pool_1)
    pool_2 = MaxPool2D(pool_size=(2, 2), strides=2)(conv_2)
 
    conv_3 = Conv2D(256, (3,3), activation = 'relu', padding='same')(pool_2)
 
    conv_4 = Conv2D(256, (3,3), activation = 'relu', padding='same')(conv_3)
    # poolig layer with kernel size (2,1)
    pool_4 = MaxPool2D(pool_size=(2, 1))(conv_4)
 
    conv_5 = Conv2D(512, (3,3), activation = 'relu', padding='same')(pool_4)
    # Batch normalization layer
    batch_norm_5 = BatchNormalization()(conv_5)
 
    conv_6 = Conv2D(512, (3,3), activation = 'relu', padding='same')(batch_norm_5)
    batch_norm_6 = BatchNormalization()(conv_6)
    pool_6 = MaxPool2D(pool_size=(2, 1))(batch_norm_6)
 
    conv_7 = Conv2D(512, (2,2), activation = 'relu')(pool_6)
 
    squeezed = Lambda(lambda x: K.squeeze(x, 1))(conv_7)
 
    # bidirectional LSTM layers with units=128
    blstm_1 = Bidirectional(LSTM(128, return_sequences=True))(squeezed)
    blstm_2 = Bidirectional(LSTM(128, return_sequences=True))(blstm_1)
 
    outputs = Dense(80, activation = 'softmax')(blstm_2)

    crnn_model = CTCModel([inputs], [outputs])

    crnn_model.compile(Adam(lr=0.0001))
    
    return crnn_model

def get_batch(paths:list, characters:list, label_data:list, train : bool, batch_size: int = 100, num_batch: int = 0):
    dict_char_list = {}
    for i in range(len(characters)):
        dict_char_list[characters[i]] = i
    
    # Loadint train data
    if train:
        start_batch = batch_size * num_batch 
        end_batch = batch_size * (num_batch + 1)
        if __name__ == '__main__':
            with Pool(4) as pool:
                result = pool.map(getting_img, paths[start_batch:end_batch])
                pool.terminate()
                pool.join()

        train_data = np.asarray(result)
        #labeling labels with numbers
        labels = list()
        words = label_data[start_batch: end_batch]
        for word in words:
            letters = list(word)
            label = list()
            for letter in letters:
                label.append(dict_char_list[letter]) 
            labels.append(label)

        label_train = sequence.pad_sequences(labels, value=float(79), dtype='float32',                                         padding="post", maxlen=21)

        len_train_labels = np.asarray([len(labels[i]) for i in range(len(labels))])
        len_train_data = np.zeros(len(result))+31.
    else:
        if __name__ == '__main__':
            with Pool(4) as pool:
                result = pool.map(getting_img_test, paths)
                pool.terminate()
                pool.join()

        train_data = np.asarray(result)
        #labeling labels with numbers
        labels = list()
        words = label_data
        for word in words:
            letters = list(word)
            label = list()
            for letter in letters:
                label.append(dict_char_list[letter]) 
            labels.append(label)

        label_train = sequence.pad_sequences(labels, value=float(79), dtype='float32',                                         padding="post", maxlen=21)

        len_train_labels = np.asarray([len(labels[i]) for i in range(len(labels))])
        len_train_data = np.zeros(len(result))+31.
        
    return train_data, label_train, len_train_data, len_train_labels

#Loading data
path = "./IAM Dataset/"
train_samples, test_samples, char_list = load_dataset(path)

#Loading the model
crnn_model = create_model()
crnn_model.load_weights('./model_weights.hdf5')
#crnn_model.summary()

